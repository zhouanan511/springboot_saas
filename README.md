# springboot_saas
Springboot+MyBatis-Plus实现多租户动态数据源模式

fork https://github.com/achievejia/springboot_saas 此仓库

##增加

增加initdatabaseLink类，启动项目时进行已有数据连接动态创建 


创建租户时，自动创建数据库，自动创建表，动态增加数据库连接 
业务上访问时在参数上加入租户ID 

##访问业务demo url 

根据租户ID，连接不同的数据库，获取user信息，达到租户数据隔离

http://localhost:8080/tUser/info?tenantId=8ff43485aff24adabfe7ac9cbf96c1a4
