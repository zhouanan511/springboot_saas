-- auto-generated definition
create table PeopleConfig
(
  id         varchar(64) not null
  comment '主键'
    primary key,
  empl_id    varchar(64) null
  comment '主键',
  phone      varchar(64) null
  comment '主键',
  name       varchar(64) null
  comment '主键',
  area_id    varchar(64) null
  comment '主键',
  area_name  varchar(64) null
  comment '主键',
  dept_id    varchar(64) null
  comment '主键',
  dept_name  varchar(64) null
  comment '主键',
  createtime varchar(64) null
  comment '主键'
);

-- auto-generated definition
create table t_user
(
  id               varchar(64) not null
  comment '主键'
    primary key,
  user_name        varchar(64) null
  comment '主键',
  password         varchar(64) null
  comment '主键',
  real_name        varchar(64) null
  comment '主键',
  id_card          varchar(64) null
  comment '主键',
  e_mail           varchar(64) null
  comment '主键',
  tel_phone        varchar(64) null
  comment '主键',
  entry_time       varchar(64) null
  comment '主键',
  resignation_time varchar(64) null
  comment '主键',
  position_id      varchar(64) null
  comment '主键',
  sex              int         null
  comment '主键',
  birthday         varchar(64) null
  comment '主键',
  operator         varchar(64) null
  comment '主键',
  dept_id          varchar(64) null
  comment '主键',
  login_time       varchar(64) null
  comment '主键',
  expire_time      varchar(64) null
  comment '主键',
  expire_status    int         null
  comment '主键',
  status           int         null
  comment '主键',
  is_admin         int         null
  comment '主键',
  create_time      varchar(64) null
  comment '主键',
  update_time      varchar(64) null
  comment '主键'
);

-- auto-generated definition
create table tenant_info
(
  id                  varchar(255) not null
  comment '主键'
    primary key,
  tenant_id           varchar(64)  null
  comment '主键',
  tenant_name         varchar(64)  null
  comment '主键',
  datasource_url      varchar(255) null
  comment '主键',
  datasource_username varchar(64)  null
  comment '主键',
  datasource_password varchar(64)  null
  comment '主键',
  datasource_driver   varchar(64)  null
  comment '主键',
  system_account      varchar(64)  null
  comment '主键',
  system_password     varchar(64)  null
  comment '主键',
  system_project      varchar(64)  null
  comment '主键',
  status              tinyint(1)   null
  comment '主键',
  create_time         timestamp    null
  comment '主键',
  update_time         timestamp    null
  comment '主键'
);

create database test1;
INSERT INTO test.t_user (id, user_name, password, real_name, id_card, e_mail, tel_phone, entry_time, resignation_time, position_id, sex, birthday, operator, dept_id, login_time, expire_time, expire_status, status, is_admin, create_time, update_time) VALUES ('1', '张三', '1', '张二愣子', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

INSERT INTO test1.t_user (id, user_name, password, real_name, id_card, e_mail, tel_phone, entry_time, resignation_time, position_id, sex, birthday, operator, dept_id, login_time, expire_time, expire_status, status, is_admin, create_time, update_time) VALUES ('1', '李四', '1', '李四喜', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);